/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
*/

#include <stdint.h>
#include <avr/pgmspace.h>
#include "SSD1306.h"
#include "font.h"



enum class TextAlign: uint8_t {
  LEFT = 0,
  RIGHT = 1,
  CENTER = 2,
  CENTER_BOTH = 3
};



class Framebuffer {
public:
    Framebuffer() {
        this->clear();
    }

    void drawBitmap(const uint8_t *progmem_bitmap, uint8_t height, uint8_t width, uint8_t pos_x, uint8_t pos_y) {
        uint8_t current_byte;
        uint8_t byte_width = (width + 7)/8;

        for (uint8_t current_y = 0; current_y < height; current_y++) {
            for (uint8_t current_x = 0; current_x < width; current_x++) {
                current_byte = pgm_read_byte(progmem_bitmap + current_y*byte_width + current_x/8);
                if (current_byte & (128 >> (current_x&7))) {
                    this->drawPixel(current_x+pos_x,current_y+pos_y,1);
                } else {
                    this->drawPixel(current_x+pos_x,current_y+pos_y,0);
                }
            }
        }
    }

    void drawBuffer(const uint8_t *progmem_buffer) {
        uint8_t current_byte;

        for (uint8_t y_pos = 0; y_pos < 64; y_pos++) {
            for (uint8_t x_pos = 0; x_pos < 128; x_pos++) {
                current_byte = pgm_read_byte(progmem_buffer + y_pos*16 + x_pos/8);
                if (current_byte & (128 >> (x_pos&7))) {
                    this->drawPixel(x_pos,y_pos,1);
                } else {
                    this->drawPixel(x_pos,y_pos,0);
                }
            }
        }
    }

    void drawPixel(uint8_t pos_x, uint8_t pos_y, uint8_t pixel = 1) {
        if (pos_x >= SSD1306_WIDTH || pos_y >= SSD1306_HEIGHT) {
            return;
        }

        if (pixel) {
            this->buffer[pos_x+(pos_y/8)*SSD1306_WIDTH] |= (1 << (pos_y&7));
        } else {
            this->buffer[pos_x+(pos_y/8)*SSD1306_WIDTH] &= ~(1 << (pos_y&7));
        }
    }

    void drawVLine(uint8_t x, uint8_t y, uint8_t length, uint8_t pixel = 1) {
        for (uint8_t i = 0; i < length; ++i) {
            this->drawPixel(x,i+y, pixel);
        }
    }

    void drawHLine(uint8_t x, uint8_t y, uint8_t length, uint8_t pixel = 1) {
        for (uint8_t i = 0; i < length; ++i) {
            this->drawPixel(i+x,y, pixel);
        }
    }

    void drawRectangle(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t pixel = 1) {
        uint8_t length = x2 - x1 + 1;
        uint8_t height = y2 - y1;

        this->drawHLine(x1,y1,length, pixel);
        this->drawHLine(x1,y2,length, pixel);
        this->drawVLine(x1,y1,height, pixel);
        this->drawVLine(x2,y1,height, pixel);
    }

    void drawFillRectangle(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, uint8_t pixel = 1) {
        uint8_t length = x2 - x1 + 1;
        uint8_t height = y2 - y1;

        for (int x = 0; x < length; ++x) {
            for (int y = 0; y <= height; ++y) {
                this->drawPixel(x1+x,y+y1, pixel);
            }
        }
    }

    void clear() {
        for (uint16_t buffer_location = 0; buffer_location < SSD1306_BUFFERSIZE; buffer_location++) {
            this->buffer[buffer_location] = 0x00;
        }
    }

    void invert(uint8_t status) {
        this->oled.invert(status);
    }

    void show() {
        this->oled.sendFramebuffer(this->buffer);
    }


void drawString(int16_t xMove, int16_t yMove, char* text, uint16_t textLength, uint16_t textWidth, TextAlign alignment) {
    const uint8_t *fontData   = ArialMT_Plain_10;

    uint8_t textHeight       = fontData[HEIGHT_POS];
    uint8_t firstChar        = fontData[FIRST_CHAR_POS];
    uint16_t sizeOfJumpTable = fontData[CHAR_NUM_POS] * JUMPTABLE_BYTES;

    uint8_t cursorX         = 0;
    uint8_t cursorY         = 0;

    switch (alignment) {
        case TextAlign::CENTER_BOTH:
            yMove -= textHeight >> 1;
            // Fallthrough
        case TextAlign::CENTER:
            xMove -= textWidth >> 1; // divide by 2
            break;
        case TextAlign::RIGHT:
            xMove -= textWidth;
            break;
        case TextAlign::LEFT:
            break;
    }

    // Don't draw anything if it is not on the screen.
    if (xMove + textWidth  < 0 || xMove > SSD1306_WIDTH ) {return;}
    if (yMove + textHeight < 0 || yMove > SSD1306_HEIGHT) {return;}

    for (uint8_t j = 0; j < textLength; j++) {
        int16_t xPos = xMove + cursorX;
        int16_t yPos = yMove + cursorY;

        uint8_t code = text[j];

        if (code >= firstChar) {
            uint16_t charCode = code - firstChar;

            // 4 Bytes per char code
            uint8_t msbJumpToChar    = pgm_read_byte( fontData + JUMPTABLE_START + charCode * JUMPTABLE_BYTES );                  // MSB  \ JumpAddress
            uint8_t lsbJumpToChar    = pgm_read_byte( fontData + JUMPTABLE_START + charCode * JUMPTABLE_BYTES + JUMPTABLE_LSB);   // LSB /
            uint8_t charByteSize     = pgm_read_byte( fontData + JUMPTABLE_START + charCode * JUMPTABLE_BYTES + JUMPTABLE_SIZE);  // Size
            uint8_t currentCharWidth = pgm_read_byte( fontData + JUMPTABLE_START + charCode * JUMPTABLE_BYTES + JUMPTABLE_WIDTH); // Width

            // Test if the char is drawable
            if (!(msbJumpToChar == 255 && lsbJumpToChar == 255)) {
                // Get the position of the char data
                uint16_t charDataPosition = JUMPTABLE_START + sizeOfJumpTable + ((msbJumpToChar << 8) + lsbJumpToChar);
                drawInternal(xPos, yPos, currentCharWidth, textHeight, fontData, charDataPosition, charByteSize);
            }

//            drawRectangle(xPos, yPos, xPos + currentCharWidth, yPos + textHeight);

            cursorX += currentCharWidth;
        }
    }
}


void drawInternal(int16_t xMove, int16_t yMove, int16_t width, int16_t height, const uint8_t *data, uint16_t offset, uint16_t bytesInData) {
    if (width < 0 || height < 0) return;
    if (yMove + height < 0 || yMove > SSD1306_HEIGHT)  return;
    if (xMove + width  < 0 || xMove > SSD1306_WIDTH)   return;

    uint8_t  rasterHeight = 1 + ((height - 1) >> 3); // fast ceil(height / 8.0)
    int8_t   yOffset      = yMove & 7;

    bytesInData = bytesInData == 0 ? width * rasterHeight : bytesInData;

    int16_t initYMove   = yMove;
    int8_t  initYOffset = yOffset;

    uint16_t i;
    for (i = 0; i < bytesInData; i++) {

        // Reset if next horizontal drawing phase is started.
        if ( i % rasterHeight == 0) {
            yMove   = initYMove;
            yOffset = initYOffset;
        }

        uint8_t currentByte = pgm_read_byte(data + offset + i);

        int16_t xPos = xMove + (i / rasterHeight);
        int16_t yPos = ((yMove >> 3) + (i % rasterHeight)) * SSD1306_WIDTH;

        //int16_t yScreenPos = yMove + yOffset;
        int16_t dataPos    = xPos  + yPos;

        if (dataPos >=  0  && dataPos < SSD1306_BUFFERSIZE &&
            xPos    >=  0  && xPos    < SSD1306_WIDTH ) {

            if (yOffset >= 0) {
                buffer[dataPos] |= currentByte << yOffset;
                buffer[dataPos] |= currentByte << yOffset;
                if (dataPos < (SSD1306_BUFFERSIZE - SSD1306_WIDTH)) {
                    buffer[dataPos + SSD1306_WIDTH] |= currentByte >> (8 - yOffset);
                }
            } else {
                // Make new offset position
                yOffset = -yOffset; 
                buffer[dataPos] |= currentByte >> yOffset;

                // Prepare for next iteration by moving one block up
                yMove -= 8;

                // and setting the new yOffset
                yOffset = 8 - yOffset;
            }
        }
    }
}


private:
    uint8_t buffer[1024];
    SSD1306 oled;
};